import { https } from "./url.service";

export const Usersev = {
 postLogin: (email, password)=>{
    return https.post("/login",{email, password})
 },
 getUser: ()=>{
    return https.get("/users?page=2");
 }

}
export default Usersev;