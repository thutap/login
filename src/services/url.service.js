import axios from "axios";

export const BASE_URL = "https://reqres.in/api";



// tạo http
export const https = axios.create({
    baseURL: BASE_URL,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
    //dùng để gọi và khai báo token khi API cần
   //headers: configHeaders(),
  });

  https.interceptors.request.use(
    (config) => {
        const token = localStorage.getItem('token');
        if (token) {
            config.headers['Authorization'] = `${token}`;
        }
        return config;
    },
    (error) => {
        return Promise.reject(error);
    }
);

// Response Interceptor: Handle the response
https.interceptors.response.use(
    (response) => {
        // Handle the response as needed
        return response;
    },
    (error) => {
        // Handle errors from the response
        return Promise.reject(error);
    }
);