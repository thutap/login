import React, { useEffect, useState } from 'react';
import { Button } from 'antd';
import Usersev from '../services/user.service';
import { useNavigate } from 'react-router-dom';
import "./user.css"
export default function UserPage() {
  const [users, SetUser] = useState([]);
  let navigate = useNavigate();

  const fetchUser = async () => {
    try {
      const res = await Usersev.getUser();
      if (res.data && Array.isArray(res.data.data)) {
        SetUser(res.data.data);
      } else {
        console.error("Response data is not an array:", res.data.data);
      }
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  useEffect(() => {
    const isLoggedIn = localStorage.getItem('isLoggedIn');
    if (!isLoggedIn) {
      navigate('/');
    } else {
      fetchUser();
    }
  }, [navigate]); 
  const handleLogout = () => {
    // Clear user authentication information (e.g., token, isLoggedIn) from localStorage
    localStorage.removeItem('token');
    localStorage.removeItem('isLoggedIn');
    
    // Navigate back to the login page
    navigate('/');
  };

  let renderUser = () => {
    return users.map((item, index) => {
      return (
        <div key={index}>
         <div >
        
        <div className='form-content'>
        <div className='form-img'>
        <img src={item.avatar} alt="" />
        </div>
         <div className='form-text'>
         <p>{item.email}</p>
          <p>{item.first_name}</p>
          <p>{item.last_name}</p>
         </div>
        </div>
         </div>
        </div>
      );
    });
  };

  return (
    <div >
       <h1>User</h1>
       <Button type="primary" onClick={handleLogout}>
        Logout
      </Button>
      <div className='form'>
      {renderUser()}
      </div>
      
   
    </div>
  );
}