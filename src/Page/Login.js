import React, { useState, useEffect } from 'react';
import { Button, Checkbox, Form, Input, message } from 'antd';
import Usersev from '../services/user.service';
import { useNavigate } from 'react-router-dom';
import './login.css';

export default function LoginPage() {
  const [rememberMe, setRememberMe] = useState(false);
  const [email, setEmail] = useState('');
  const [submitButtonDisabled, setSubmitButtonDisabled] = useState(true); // Initialize as true
  let navigate = useNavigate();

  useEffect(() => {
    // Check if the user is already logged in
    const isLoggedIn = localStorage.getItem('isLoggedIn') === 'true';

    if (isLoggedIn) {
      // If already logged in, redirect to the home page
      navigate('/home');
    } else {
      // Check if there is a stored email in local storage
      const storedEmail = localStorage.getItem('rememberedEmail');

      if (storedEmail) {
        setRememberMe(true);
        setEmail(storedEmail);
        // Enable the button if there is a stored email
        setSubmitButtonDisabled(false);
      }
    }
  }, [navigate]);

  const handleLogin = (password) => {
    console.log('Email:', email);
    console.log('Password:', password);
  
    Usersev.postLogin(email, password)
      .then((response) => {
        if (response.data && response.data.token) {
          localStorage.setItem('token', response.data.token);
          localStorage.setItem('isLoggedIn', 'true');
  
          // If "Remember me" is checked, store the email in local storage
          if (rememberMe) {
            localStorage.setItem('rememberedEmail', email);
          } else {
            // If "Remember me" is unchecked, remove the stored email
            localStorage.removeItem('rememberedEmail');
          }
  
          message.success('Đăng nhập thành công');
          navigate('/home');
        } else {
          message.error('Đăng nhập không thành công. Vui lòng kiểm tra thông tin đăng nhập.');
        }
      })
      .catch((error) => {
        console.error('Error from API:', error);
        message.error('Đã xảy ra lỗi trong quá trình đăng nhập. Vui lòng thử lại.');
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
    setSubmitButtonDisabled(true);
  };

  const onFinish = (values) => {
    console.log('values:', values);
    const { password } = values;
    handleLogin(password);
  };

  // Update submit button disabled state when email changes
  const onEmailChange = (e) => {
    const newEmail = e.target.value;
    setEmail(newEmail);
    // Enable the button if email is not empty
    setSubmitButtonDisabled(newEmail === '');
  };

  return (
    <div className='form-login'>
      <div className='form-item'>
        <h2>Đăng nhập</h2>
        <div>
          <Form
            name='basic'
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 16,
            }}
            style={{
              maxWidth: 600,
            }}
            initialValues={{
              email: localStorage.getItem('rememberedEmail') || '',
              remember: rememberMe,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete='off'
          >
            <Form.Item
              label='Email'
              name='email'
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập địa chỉ email',
                },
              ]}
            >
              <Input onChange={onEmailChange} />
            </Form.Item>

            <Form.Item
              label='Mật khẩu'
              name='password'
              rules={[
                {
                  required: true,
                  message: 'Vui lòng nhập mật khẩu!',
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              name='remember'
              valuePropName='checked'
              wrapperCol={{
                offset: 8,
                span: 16,
              }}
            >
              <Checkbox onChange={(e) => setRememberMe(e.target.checked)}>Remember me</Checkbox>
            </Form.Item>

            <Form.Item
              wrapperCol={{
                offset: 8,
                span: 16,
              }}
            >
              <Button type='primary' htmlType='submit' disabled={submitButtonDisabled}>
                Gửi
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}

