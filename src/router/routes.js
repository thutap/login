import LoginPage from "../Page/Login";
 import UserPage from "../Page/UserPage";

export const routes = [
  {
    path: "/",
    component: <LoginPage/>,
  },
  {
    path: "/home",
    component: <UserPage/>,
  },

];